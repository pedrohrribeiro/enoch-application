from selenium import webdriver

page_list = [
  'http://scobleizer.com/',
  'http://www.precentral.net/',
  'http://www.cultofmac.com/',
  'http://moconews.net/',
  'http://gizmodo.com/'
]

driver = webdriver.Firefox()
driver.set_page_load_timeout(30)

def Test():
  for page in page_list: 
    driver.get(page)

if __name__ == '__main__':
    import timeit
    print(timeit.timeit("Test()", setup="from __main__ import Test", number=1))
