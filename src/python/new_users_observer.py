'''
Created on Nov 10, 2014

@author: ribeiro
'''
import time
import re
#import tailer
from threading import Thread
from netaddr.ip import IPAddress
import tailer

NEW_USERS_IPTABLES_LOG_PATH='/var/log/nets_new_users.log'


class NewUsersObserverThread(Thread):

    def __init__(self, application, system_data, terminate_event):
        Thread.__init__(self)
        self.daemon = True
        self._application = application
        self._system_data = system_data
        self._terminate_event = terminate_event


    def _IsAValidLANIPAddress(self, ip_address_str):
        ip_address = IPAddress(ip_address_str)
        for ip_network in self._system_data.GetLanNetworks():
            if ip_address in ip_network:
                return True
        return False


    def run(self):
        regexp = re.compile('.*PHYSIN=%s.*SRC=([^ ]+) .*' %self._system_data.GetLanInterface())
        
        users = self._system_data.GetUsers()
        
        for new_entry in tailer.follow(open(NEW_USERS_IPTABLES_LOG_PATH)):

            match = regexp.match(new_entry)
            if match is not None:

                source_ip = match.group(1)

                if self._IsAValidLANIPAddress(source_ip) and users.GetUserByIPAddress(source_ip) is None:

                    self._application.AddNewUser(source_ip)
                

    def Follow(self, a_file, delay=1.0):
        trailing = True

        line_terminators = ('\r\n', '\n', '\r')

        def seek(pos, whence=0):
                a_file.seek(pos, whence)

        while not self._terminate_event.is_set():
            where = a_file.tell()
            line = a_file.readline()
            if line:        
                if trailing and line in line_terminators:
                    # This is just the line terminator added to the end of the a_file
                    # before a new line, ignore.
                    trailing = False
                    continue

                if line[-1] in line_terminators:
                    line = line[:-1]
                    if line[-1:] == '\r\n' and '\r\n' in line_terminators:
                        # found crlf
                        line = line[:-1]

                trailing = False
                yield line
            else:
                trailing = True
                seek(where)
                time.sleep(delay) 
