'''
Created on Nov 10, 2014

@author: ribeiro
'''
from threading import Thread
import re
import time
import os
from rate import Rate
from configuration import LAN_IFACE, WAN_IFACE, STATISTICS_POOLING_PERIOD,\
    ENTRANCE_CLASS_ID


class StatisticsCollector(Thread):
    '''
    This Thread class collects data from HTB classes in all given network interface cards
    at a regular interval and writes the data to the given queue.
    '''

    REGEXP = re.compile('class htb 1:(\d+).*rate (\d+)(\w+) ceil (\d+)(\w+).*\n Sent.*\n rate (\d+)(\w+).*')
    TC_CMD_TEMPLATE = 'tc -s -d class show dev %s'
    ROOT_CLASS_ID = '1'
    
    def __init__(self, system_data, terminate_event):
        Thread.__init__(self)
        self._lan_iface = LAN_IFACE
        self._wan_iface = WAN_IFACE
        self._system_data = system_data
        self._terminate_event = terminate_event


    def run(self):
        system_data = self._system_data
        
        while not self._terminate_event.is_set():

            lan_iface_statistics = self.GetNICStatistics(self._lan_iface)
            wan_iface_statistics = self.GetNICStatistics(self._wan_iface)
            
            if len(lan_iface_statistics) == 0 or len(wan_iface_statistics) == 0:
                time.sleep(5)
                continue
            
            system_data.SetTotalInstantDownloadRate(lan_iface_statistics[self.ROOT_CLASS_ID]['instant_rate'])
            system_data.SetTotalInstantUploadRate(wan_iface_statistics[self.ROOT_CLASS_ID]['instant_rate'])
                        

            users = system_data.GetUsers()

            for user_id, download_statistics in lan_iface_statistics.iteritems():
                if user_id != self.ROOT_CLASS_ID and user_id != ENTRANCE_CLASS_ID:
                    upload_statistics = wan_iface_statistics[user_id]

                    user = users.GetUserById(user_id)

                    user.SetInstantDownloadRate(download_statistics['instant_rate'])
                    user.SetInstantUploadRate(upload_statistics['instant_rate'])
                    print user

            time.sleep(STATISTICS_POOLING_PERIOD)


    def GetNICStatistics(self, nic):
        nic_data = {}

        stdin, stdout = os.popen2(self.TC_CMD_TEMPLATE %nic)
        output = stdout.read()
        data = self.REGEXP.findall(output)

        for item in data:
            user_id, set_rate_value, set_rate_unit, ceil_rate_value, ceil_rate_unit, instant_rate_value, instant_rate_unit = item
            nic_data[user_id] = {
                'configured_rate': Rate(float(set_rate_value), set_rate_unit),
                'configured_ceil': Rate(float(ceil_rate_value), ceil_rate_unit),
                'instant_rate': Rate(float(instant_rate_value), instant_rate_unit),
            }

        return nic_data
