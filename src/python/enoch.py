from threading import Lock, Event
from netaddr import IPNetwork
from statistics_collector import StatisticsCollector 
from new_users_observer import NewUsersObserverThread
from user import UsersManager
from web_socket_server import EnochWebSocketServerThread
from bandwidth_manager import BandwidthManager
from operating_system_manager import OperatingSystemManagerThread
from configuration import LAN_NETWORKS
from system_data import SystemData


class EnochApplication:

    STDOUT_LOG_PATH='/dev/null'
    STDERR_LOG_PATH='/dev/null'

    def __init__(self):
        self._users_data = {} 
        # Maps user IP to a tuple 
        # (user_id, download_rate, download_ceil, upload_rate, upload_ceil)
        self.stdin_path = '/dev/null'
        self.stdout_path = self.STDOUT_LOG_PATH
        self.stderr_path = self.STDERR_LOG_PATH
        self.pidfile_path =  '/var/run/enochd/enochd.pid'
        self.pidfile_timeout = 5

        self._lock = Lock()
        
        self._terminate_event = Event()
        
        self._system_data = SystemData()
        
        self._bandwidth_manager = BandwidthManager(self._system_data)
        
        self._operating_system_manager_thread = OperatingSystemManagerThread(self._system_data, self._terminate_event)

        self._new_users_observer_thread = NewUsersObserverThread(self, self._system_data, self._terminate_event)

        self._network_statistics_collector_thread = StatisticsCollector(self._system_data, self._terminate_event)

        self._web_socket_server_thread = EnochWebSocketServerThread(self)
    
    
    def AcquireLock(self):
        self._lock.acquire()
        
    
    def ReleaseLock(self):
        self._lock.release()
    
    
    def AddNewUser(self, ip_address):
        self.AcquireLock()
        
        users = self._system_data.GetUsers()
        
        users.CreateNewUser(ip_address)
        self._bandwidth_manager.Redistribute()
        
        self.ReleaseLock()
        
        self._operating_system_manager_thread.ScheaduleReconfigure()
        
    
    def RegisterHandler(self, handler):
        handler.Scheadule(handler.write_message, ('aloooo',), {})
        

    def Main(self):
        self._operating_system_manager_thread.start()
        self._new_users_observer_thread.start()
        self._network_statistics_collector_thread.start()
        self._web_socket_server_thread.start()
        
        self._web_socket_server_thread.join()


if __name__ == '__main__':
    import logging


    logger = logging.getLogger("NetSplitterLog")
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter("%(asctime)s - %(levelname)s: %(message)s")
    handler = logging.FileHandler("/var/log/enochd/enochd.log")
    handler.setFormatter(formatter)
    logger.addHandler(handler)


    #from daemon import runner
    #import signal

    #app = NetSDaemon()


    #def AppCleanup(signum, frame):
    #    app.Cleanup()
    #    daemon_runner.daemon_context.terminate(signum, frame)


    #daemon_runner = runner.DaemonRunner(app)
    #This ensures that the logger file handle does not get closed during daemonization
    #daemon_runner.daemon_context.files_preserve=[handler.stream]
    #daemon_runner.daemon_context.signal_map = { signal.SIGTERM: AppCleanup }
    #daemon_runner.do_action()


    app = EnochApplication()
    app.Main()
