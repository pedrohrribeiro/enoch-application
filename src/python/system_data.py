from constants import TOTAL_RATE_UNIT
from configuration import USER_RATE_UNIT, DOWNLOAD_LINK_RATE, UPLOAD_LINK_RATE,\
    DEFAULT_PRIORITY_PERIOD, MAX_PRIORITY_USERS_COUNT, LAN_IFACE, WAN_IFACE,\
    LAN_NETWORKS
from rate import Rate
from user import UsersManager
from acccessors import Accessors



class SystemData:
    
    __metaclass__ = Accessors
    _READ_WRITE = [
        'lan_interface',
        'wan_interface',
        'lan_networks',
        'download_link_rate',
        'upload_link_rate',
        'total_instant_download_rate', 
        'total_instant_upload_rate', 
        ]
    _READ = ['users']
        
    def __init__(self):
        self.lan_interface = LAN_IFACE
        self.wan_interface = WAN_IFACE
        
        self.lan_networks = LAN_NETWORKS
        
        self.total_rate_unit = TOTAL_RATE_UNIT
        self.user_rate_unit = USER_RATE_UNIT
        self.download_link_rate = DOWNLOAD_LINK_RATE
        self.upload_link_rate = UPLOAD_LINK_RATE
        self.default_priority_period = DEFAULT_PRIORITY_PERIOD
        self.max_priority_users_count = MAX_PRIORITY_USERS_COUNT
        
        self.total_instant_download_rate = Rate(0.0, 'kbps')
        self.total_instant_upload_rate = Rate(0.0, 'kbps')

        self.users = UsersManager()
        
    
    def GetActiveUsersCount(self):
        return len(self.users)
    
    
    def GetTotalInstantDownloadPercentage(self):
        return 100. * self.total_instant_download_rate.GetValue('kbps') / self.download_link_rate.GetValue('kbps')

    
    def GetTotalInstantUploadPercentage(self):
        return 100. * self.total_instant_upload_rate.GetValue('kbps') / self.upload_link_rate.GetValue('kbps')
    
