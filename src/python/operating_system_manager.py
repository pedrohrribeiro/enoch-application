from string import Template
import logging
import os
from threading import Thread, Lock
import time
from configuration import LAN_IFACE, WAN_IFACE, DOWNLOAD_LINK_RATE,\
    UPLOAD_LINK_RATE, ENTRANCE_CLASS_ID

logger = logging.getLogger("NetSplitterLog")


class OperatingSystemManagerThread(Thread):
    def __init__(self, system_data, terminate_event):
        Thread.__init__(self)
        self._terminate_event = terminate_event
        self._system_data = system_data
        self._lock = Lock()
        self._configured_users_ips = []
        self._scheaduled_reconfigure = False
    
    
    def run(self):
        self.Setup()
        
        while not self._terminate_event.is_set():
            if self._scheaduled_reconfigure:
                self._lock.acquire()
                self._Reconfigure()
                self._scheaduled_reconfigure = False
                self._lock.release()
            else:
                time.sleep(.1)
        
        self.Cleanup()
    
    
    def ScheaduleReconfigure(self):
        self._lock.acquire()
        self._scheaduled_reconfigure = True
        self._lock.release()
    
    
    def _Reconfigure(self):
        user_ips = []
        for user in self._system_data.GetUsers():
            if user.ip_address not in self._configured_users_ips:
                self._ConfigureNewUser(user)
                self._configured_users_ips.append(user.ip_address)
            else:
                self._ReconfigureUser(user)
                
            user_ips.append(user.ip_address)
        
        removed_users = list(set(self._configured_users_ips) - set(user_ips))
        
        for user_ip in removed_users:
            self._RemoveUserConfiguration(user_ip)
            self._configured_users_ips.remove(user_ip)
            

    def _ConfigureNewUser(self, user):

        command_template = Template(
'''
tc class add dev $lan_iface parent 1:1 classid 1:$user_id htb rate ${download_rate}Kbit ceil ${download_ceil}Kbit
tc qdisc add dev $lan_iface parent 1:$user_id fq_codel

# The following two rules go in inverse order because af the -I (including on top)
iptables -t mangle -I FORWARD -d $user_ip -j ACCEPT
iptables -t mangle -I FORWARD -d $user_ip -j MARK --set-mark $hex_user_id

tc filter add dev $lan_iface parent 1: handle $hex_user_id fw flowid 1:$user_id
  
tc class add dev $wan_iface parent 1:1 classid 1:$user_id htb rate ${upload_rate}Kbit ceil ${upload_ceil}Kbit
tc qdisc add dev $wan_iface parent 1:$user_id fq_codel

iptables -t mangle -I FORWARD -s $user_ip -j ACCEPT
iptables -t mangle -I FORWARD -s $user_ip -j MARK --set-mark $hex_user_id 

tc filter add dev $wan_iface parent 1: handle $hex_user_id fw flowid 1:$user_id
''')
        
        values = {
            'user_id' : user.id,
            'hex_user_id' : str(hex(int(user.id))),
            'user_ip' : user.ip_address,
            'lan_iface' : LAN_IFACE,
            'wan_iface' : WAN_IFACE,
            'download_rate' : user.configured_download_rate.GetValue('Kbit'),
            'download_ceil' : user.configured_download_ceil.GetValue('Kbit'),
            'upload_rate' : user.configured_upload_rate.GetValue('Kbit'),
            'upload_ceil' : user.configured_upload_ceil.GetValue('Kbit')
        }
        
        command = command_template.substitute(**values)

        self._ExecuteCommand(command)

        log_message_template = Template('New user: IP=${user_ip} DOWN_RATE=${download_rate}Kbit DOWN_CEIL=${download_ceil}Kbit UP_RATE=${upload_rate}Kbit UP_CEIL=${upload_ceil}Kbit')
        
        message = log_message_template.substitute(**values)
        
        logger.info(message)


    def Setup(self):
        logger.info("########### Starting ##########")
        self._DeleteAllRules()
        self._SetupFixedRules()
        logger.info("All fixed fw and shaping rules set")


    def Cleanup(self):
        self._DeleteAllRules()
        log_message = 'Terminating...Bye.'
        logger.info(log_message)


    def _DeleteAllRules(self):
        command = '''
tc qdisc del dev eth0 root
tc qdisc del dev eth1 root
iptables -t mangle -F FORWARD'''
        self._ExecuteCommand(command)


    def _SetupFixedRules(self):
        command_template = Template('''
tc qdisc add dev $lan_iface root handle 1: htb default $entrance_class_id
tc class add dev $lan_iface parent 1: classid 1:1 htb rate ${download_link_rate}Kbit ceil ${download_link_ceil}Kbit

tc class add dev $lan_iface parent 1:1 classid 1:$entrance_class_id htb rate ${download_link_rate}Kbit ceil ${download_link_ceil}Kbit
tc qdisc add dev $lan_iface parent 1:$entrance_class_id fq_codel

tc qdisc add dev $wan_iface root handle 1: htb default $entrance_class_id
tc class add dev $wan_iface parent 1: classid 1:1 htb rate ${upload_link_rate}Kbit ceil ${upload_link_ceil}Kbit

tc class add dev $wan_iface parent 1:1 classid 1:$entrance_class_id htb rate ${upload_link_rate}Kbit ceil ${upload_link_ceil}Kbit 
tc qdisc add dev $wan_iface parent 1:$entrance_class_id fq_codel

iptables -t mangle -A FORWARD  -m limit --limit 10/sec  -j LOG --log-prefix "[NEW USER PKT]" --log-level 4
''')
        command = command_template.substitute(
                lan_iface=LAN_IFACE,
                wan_iface=WAN_IFACE,
                download_link_rate=DOWNLOAD_LINK_RATE.GetValue('Kbit'),
                download_link_ceil=DOWNLOAD_LINK_RATE.GetValue('Kbit'),
                upload_link_rate=UPLOAD_LINK_RATE.GetValue('Kbit'),
                upload_link_ceil=UPLOAD_LINK_RATE.GetValue('Kbit'),
                entrance_class_id=ENTRANCE_CLASS_ID
        )

        self._ExecuteCommand(command)


    def _ReconfigureUser(self, user):
        command_template = Template(
'''
tc class change dev $lan_iface parent 1:1 classid 1:$user_id htb rate ${download_rate}Kbit ceil ${download_ceil}Kbit
tc class change dev $wan_iface parent 1:1 classid 1:$user_id htb rate ${upload_rate}Kbit ceil ${upload_ceil}Kbit
''')
        values = {
            'user_id' : user.id,
            'user_ip' : user.ip_address,
            'lan_iface' : LAN_IFACE,
            'wan_iface' : WAN_IFACE,
            'download_rate' : user.configured_download_rate.GetValue('Kbit'),
            'download_ceil' : user.configured_download_ceil.GetValue('Kbit'),
            'upload_rate' : user.configured_upload_rate.GetValue('Kbit'),
            'upload_ceil' : user.configured_upload_ceil.GetValue('Kbit')
        }
        
        command = command_template.substitute(**values)

        self._ExecuteCommand(command)
        
        log_message_template = Template('Changed user: IP=${user_ip} DOWN_RATE=${download_rate}Kbit DOWN_CEIL=${download_ceil}Kbit UP_RATE=${upload_rate}Kbit UP_CEIL=${upload_ceil}Kbit')
        
        message = log_message_template.substitute(**values)
        
        logger.info(message)


    def _ExecuteCommand(self, command):
        print command
        os.popen(command)
