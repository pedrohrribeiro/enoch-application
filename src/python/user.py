import re
from rate import Rate
from acccessors import Accessors


class User:
    __metaclass__ = Accessors
    _READ_WRITE = [
        'instant_download_rate',
        'instant_upload_rate'
        ]
    _READ = ['users']
    
    def __init__(self, user_id, ip_address):
        self.id = user_id
        self.ip_address = ip_address
        
        self.rank = None
        
        # Configured rate attributes
        self.configured_download_rate = Rate(0.0, 'kbps')
        self.configured_download_ceil = Rate(0.0, 'kbps')
        
        self.configured_upload_rate = Rate(0.0, 'kbps')
        self.configured_upload_ceil = Rate(0.0, 'kbps')
        
        # Instant rate attributes
        self.instant_download_rate = Rate(0.0, 'kbps')
        self.instant_download_percentage = 0.0
 
        self.instant_upload_rate = Rate(0.0, 'kbps')
        self.instant_upload_percentage = 0.0
        
        # Priority attributes
        self.has_priority = False
        self.priority_start_time = None
        self.priority_end_time = None
        self.priority_remaining_time = None
        self.priority_remaining_percentage = None
    
    def __str__(self):
        return '%s %s' %(self.instant_download_rate.GetValue('kbps'), self.instant_upload_rate.GetValue('kbps'))


class UsersManager:
    
    END_OF_IP_REGEX = re.compile('.*\.(.*)\.(.*)$')
    
    def __init__(self):
        self._users_by_id = {}
        self._users_by_ip_address = {}
        self._last_id = 99
    
    
    def _GetIdFromIPAddress(self, ip_address):
        matches = self.END_OF_IP_REGEX.findall(ip_address)[0]
        return int(matches[0] + matches[1])
        
    
    def _AddNewUser(self, user):
        self._users_by_id[user.id] = user
        self._users_by_ip_address[user.ip_address] = user.ip_address
    
    
    def CreateNewUser(self, ip_address):
        print 'CreateNewUser', ip_address
        self._last_id += 1
        user = User(str(self._last_id), ip_address)
        self._AddNewUser(user)
        return user
    
    
    def GetUserById(self, user_id):
        try:
            return self._users_by_id[user_id]
        except KeyError:
            return None
    
    
    def GetUserByIPAddress(self, ip_address):
        try:
            return self._users_by_ip_address[ip_address]
        except KeyError:
            return None
    
    
    def __iter__(self):
        return self._users_by_id.itervalues()
        