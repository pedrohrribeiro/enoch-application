'''
Created on Jan 29, 2015

@author: phrribeiro
'''
import unittest
from user import UsersManager
from bandwidth_manager import BandwidthManager
from rate import Rate
from system_data import SystemData


class Test(unittest.TestCase):


    def testBandwidthManager(self):
        
        system_data = SystemData()
        
        users = system_data.GetUsers()
        
        user1 = users.CreateNewUser('192.168.1.100')
        user2 = users.CreateNewUser('192.168.1.101')
        
        download_link_rate = Rate(10., 'Mbit')
        upload_link_rate = Rate(5., 'Mbit')
        
        manager = BandwidthManager(system_data)

        for user in (user1, user2):        
            self.assertEquals(user.configured_download_rate.GetValue('kbps'), 0.0)
            self.assertEquals(user.configured_download_ceil.GetValue('kbps'), 0.0)
            self.assertEquals(user.configured_upload_rate.GetValue('kbps'), 0.0)
            self.assertEquals(user.configured_upload_ceil.GetValue('kbps'), 0.0)
        
        manager.Redistribute()
        
        
        
        expected_download_rate = (download_link_rate.GetValue('kbps') - manager.GetEntranceClassDownloadRate().GetValue('kbps'))/ 2.0
        expected_upload_rate = (upload_link_rate.GetValue('kbps') - manager.GetEntranceClassUploadRate().GetValue('kbps'))/ 2.0
        
        for user in (user1, user2):
            self.assertEquals(user.configured_download_rate.GetValue('kbps'), expected_download_rate)
            self.assertEquals(user.configured_download_ceil.GetValue('kbps'), download_link_rate.GetValue('kbps'))
            self.assertEquals(user.configured_upload_rate.GetValue('kbps'), expected_upload_rate)
            self.assertEquals(user.configured_upload_ceil.GetValue('kbps'), upload_link_rate.GetValue('kbps'))
            
        
        user1.has_priority = True
        
        manager.Redistribute()
        
        # Nothing shold have changed here since the number of active users is only 2
        
        for user in (user1, user2):
            self.assertEquals(user.configured_download_rate.GetValue('kbps'), expected_download_rate)
            self.assertEquals(user.configured_download_ceil.GetValue('kbps'), download_link_rate.GetValue('kbps'))
            self.assertEquals(user.configured_upload_rate.GetValue('kbps'), expected_upload_rate)
            self.assertEquals(user.configured_upload_ceil.GetValue('kbps'), upload_link_rate.GetValue('kbps'))
        
        
        for i in range(11):
            ip_address = '192.168.10.%d' %i
            users.CreateNewUser(ip_address)
            
        manager.Redistribute()
        
        self.assertEquals(
            user1.configured_download_rate.GetValue('kbps'), manager.DOWNLOAD_RATE_FOR_PRIORITY_USERS.GetValue('kbps'))
        self.assertEquals(user1.configured_download_ceil.GetValue('kbps'), download_link_rate.GetValue('kbps'))
        
        self.assertEquals(
            user1.configured_upload_rate.GetValue('kbps'), manager.UPLOAD_RATE_FOR_PRIORITY_USERS.GetValue('kbps'))
        self.assertEquals(user1.configured_upload_ceil.GetValue('kbps'), upload_link_rate.GetValue('kbps'))
        
        number_of_users_without_priority = 2 + 11 - 1
        # First two, the other eleven added, less one with priority
        
        
        user2_expected_download_rate = \
            (download_link_rate.GetValue('kbps') - manager.GetEntranceClassDownloadRate().GetValue('kbps') - manager.DOWNLOAD_RATE_FOR_PRIORITY_USERS.GetValue('kbps')) / number_of_users_without_priority
        
        user2_expected_upload_rate = \
            (upload_link_rate.GetValue('kbps') - manager.GetEntranceClassUploadRate().GetValue('kbps') - manager.UPLOAD_RATE_FOR_PRIORITY_USERS.GetValue('kbps')) / number_of_users_without_priority
        
            
        self.assertEquals(
            user2.configured_download_rate.GetValue('kbps'), user2_expected_download_rate)
        self.assertEquals(user1.configured_download_ceil.GetValue('kbps'), download_link_rate.GetValue('kbps'))
        
        self.assertEquals(
            user2.configured_upload_rate.GetValue('kbps'), user2_expected_upload_rate)
        self.assertEquals(user1.configured_upload_ceil.GetValue('kbps'), upload_link_rate.GetValue('kbps'))
        


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testBandwidthManager']
    unittest.main()