from configuration import ENTRANCE_CLASS_RATE, DOWNLOAD_RATE_FOR_PRIORITY_USERS,\
    UPLOAD_RATE_FOR_PRIORITY_USERS



class BandwidthManager:

    
    def __init__(self, system_data):
        self._system_data = system_data
    
    
    def Redistribute(self):
        system_data = self._system_data
        
        download_link_rate = system_data.GetDownloadLinkRate()
        upload_link_rate = system_data.GetUploadLinkRate()
        
        users = list(system_data.GetUsers())

        
        remaining_download_bandwidth_kbps = download_link_rate.GetValue('kbps') - \
            self.GetEntranceClassDownloadRate().GetValue('kbps')
            
        remaining_upload_bandwidth_kbps = upload_link_rate.GetValue('kbps') - \
            self.GetEntranceClassUploadRate().GetValue('kbps')
        
        users_count = len(users)
        
        # We must check if there is  enough users at this moment that we really
        # need make a special reserve
        
        make_special_reserve_for_users_with_priority = True
        
        if remaining_download_bandwidth_kbps / (users_count) >= DOWNLOAD_RATE_FOR_PRIORITY_USERS.GetValue('kbps') and \
          remaining_upload_bandwidth_kbps / (users_count) >= UPLOAD_RATE_FOR_PRIORITY_USERS.GetValue('kbps'):

            make_special_reserve_for_users_with_priority = False  
        
        if make_special_reserve_for_users_with_priority:
            # Users with priority gets more rate
            for user in users[:]:
                if user.has_priority:
                    
                    user_download_rate_kbps = DOWNLOAD_RATE_FOR_PRIORITY_USERS.GetValue('kbps')
            
                    user.configured_download_rate.SetValue(user_download_rate_kbps, 'kbps')
                    
                    remaining_download_bandwidth_kbps -= user_download_rate_kbps 
                    
                    user.configured_download_ceil.SetValue(download_link_rate.GetValue('kbps'), 'kbps')
                    
                    user_upload_rate_kbps = UPLOAD_RATE_FOR_PRIORITY_USERS.GetValue('kbps')
            
                    user.configured_upload_rate.SetValue(user_upload_rate_kbps, 'kbps')
                    
                    remaining_upload_bandwidth_kbps -= user_upload_rate_kbps
                    
                    user.configured_upload_ceil.SetValue(upload_link_rate.GetValue('kbps'), 'kbps')
                    
                    users.remove(user)
                    
        
        # Rest of the available bandwidth is divided equally!y between the other
        # users
        divisor = len(users)
        
        user_download_rate_kbps = remaining_download_bandwidth_kbps / divisor
        user_upload_rate_kbps = remaining_upload_bandwidth_kbps / divisor
        
        for user in users:
            user.configured_download_rate.SetValue(user_download_rate_kbps, 'kbps')
            user.configured_download_ceil.SetValue(download_link_rate.GetValue('kbps'), 'kbps')
            
            user.configured_upload_rate.SetValue(user_upload_rate_kbps, 'kbps')
            user.configured_upload_ceil.SetValue(upload_link_rate.GetValue('kbps'), 'kbps') 
        
    
    def GetEntranceClassDownloadRate(self):
        return ENTRANCE_CLASS_RATE
    
    
    def GetEntranceClassUploadRate(self):
        return ENTRANCE_CLASS_RATE

        