class Rate:
    def __init__(self, value, unit):
        # Always keep the value in kbps
        self._value = None
        self.SetValue(value, unit)


    def SetValue(self, value, unit):
        if unit == 'Mbit':
            self._value = 125. * value
        elif unit == 'bit':
            self._value = value / 8000. 
        elif unit == 'Kbit':
            self._value = value / 8. 
        elif unit == 'kbps':
            self._value = value
        else:
            raise ValueError('Unknown unit %s' %unit)


    def GetValue(self, unit):
        if unit == 'Mbit':
            return self._value / 125.
        elif unit == 'bit':
            return self._value * 8000. 
        elif unit == 'Kbit':
            return self._value * 8. 
        elif unit == 'kbps':
            return self._value
        else:
            raise ValueError('Unknown unit %s' %unit)
