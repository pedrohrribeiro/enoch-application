from tornado.websocket import WebSocketHandler
from threading import Thread
from tornado.ioloop import PeriodicCallback


 
class EnochWebSocketHandler(WebSocketHandler):
    
    def __init__(self, application, request, **kwargs):
        self._periodic_callback = PeriodicCallback(self._ExecuteScheaduled, 500)
        self._scheaduled = []
        
        WebSocketHandler.__init__(self, application, request, **kwargs)
        


    def initialize(self, enoch_application):
        '''
        Called at the end of WebSocketHandler.__init__() with the parameters
        given when it is registered.
        '''
        WebSocketHandler.initialize(self)
        
        self.enoch_application = enoch_application
        
        self.enoch_application.RegisterHandler(self)
        
        self._periodic_callback.start()
        
    
    def Scheadule(self, method, args=None, kwargs=None):
        '''
        Scheadule calls for this handler to be executed inside the application 
        loop.
        '''
        self._scheaduled.append((method, args, kwargs))
    
    
    def _ExecuteScheaduled(self):
        '''
        Execute all scheaduled calls.
        '''
        while len(self._scheaduled) != 0:
            method, args, kwargs = self._scheaduled.pop(0)
            method(*args, **kwargs)


    def open(self):
        print 'Openned!'
        
        
    def on_message(self, message):
        a = self.enoch_application.Setup()
        print a, message



class EnochWebSocketServerThread(Thread):
    
    def __init__(self, enoch_application):
        self.enoch_application = enoch_application
        Thread.__init__(self)
        self.daemon = True
        
        
    def run(self):
        import tornado
        import tornado.httpserver
        
        application = tornado.web.Application([
            (r'/ws', EnochWebSocketHandler, {'enoch_application' : self.enoch_application}),
        ])
 
        http_server = tornado.httpserver.HTTPServer(application)
        http_server.listen(8082)
        tornado.ioloop.IOLoop.instance().start()
