'''
Created on Jan 26, 2015

@author: phrribeiro
'''
import unittest
from rate import Rate


class Test(unittest.TestCase):


    def testRate(self):
        
        a_rate = Rate(100.0, 'kbps')
        
        self.assertAlmostEqual(a_rate.GetValue('Mbit'), .8)
        self.assertAlmostEqual(a_rate.GetValue('Kbit'), 800.)
        self.assertAlmostEqual(a_rate.GetValue('bit'), 800000.)
        
        a_rate.SetValue(1., 'Mbit')
        self.assertAlmostEqual(a_rate.GetValue('kbps'), .8)
        self.assertAlmostEqual(a_rate.GetValue('Kbit'), 800.)
        self.assertAlmostEqual(a_rate.GetValue('bit'), 800000.)
        
        
        
        


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testRate']
    unittest.main()